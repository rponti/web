# README #

### What is this repository for? ###

* This repository is for practicing web automation

### How do I get set up? ###

* Setup Ruby
* Setup Cucumber
* Setup Capybara
* Clone repository
* how to run tests:  bundle exec cucumber features/ 
* profit

### Contribution guidelines ###

* Follow Capybara guidelines when writing tests

### Who do I talk to? ###

* Repo owner:  Rob Ponti