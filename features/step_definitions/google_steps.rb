Given(/^I navigate to google in (.+)$/) do |size|
	ResponsiveHelpers.set_size(size)
	visit 'https://www.google.com/'
end

And(/^I type (.+)$/) do |search_term|
	within '#tsf' do
		fill_in 'lst-ib', :with => search_term
	end
end

When(/^I click search$/) do
	click_button 'Search'
end

Then(/^I see search results$/) do
	expect(page).to have_css('.rc')
end