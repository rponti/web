Given(/^I navigate to twitter in (.+)$/) do |size|
	ResponsiveHelpers.set_size(size)
	visit 'https://twitter.com/?lang=en'
end

When(/^I click "([^\"]*)" in the header$/) do |btn|
	click_button btn
end

When(/^I enter my "([^\"]*)"$/) do |username|
	within '#login-dialog-dialog' do
		fill_in 'session[username_or_email]', :with => username
	end 
end

When(/^I enter my password$/) do
	within '#login-dialog-dialog' do
		fill_in 'session[password]', :with => 'Passw0rd'
	end	
end

When(/^I click "([^"]*)" in the dialog$/) do |btn|
	click_button btn
end

Then(/^I am logged into Twitter$/) do
	within '#global-actions' do
		expect(page).to have_text("Home")
	end
end