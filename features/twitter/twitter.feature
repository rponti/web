Feature: Twitter login

Scenario Outline: I can login into Twitter
	Given I navigate to twitter in <size>
	When I click "Log In" in the header
	And I enter my "<username>"
	And I enter my password
	And I click "Log in" in the dialog
	Then I am logged into Twitter

	Examples:
		| size	  | username    |
		| desktop | tester_2007 |
		| tablet  | tester_2007 |
		| mobile  | tester_2007 |