Feature: Google Search

Scenario Outline: Search for March Madness on Google
	Given I navigate to google in <size>
	And I type "2016 March Madness Tournament"
	When I click search
	Then I see search results

	Examples:
		| size	  |
		| desktop |
		| tablet  |
		| mobile  |